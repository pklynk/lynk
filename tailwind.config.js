module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        yellow: {
          1: "#FFA765",
          2: "#F17930",
        },
        red: {
          1: "#F84A4B"
        },
        grey: {
          1: "#E5E5E5",
          2: "#F0EADE",
          3: "#DFD4B6"
        },
      },
      height:{
        100:"30rem"
      },
      boxShadow:{
        DEFAULT: "0 0 20px 5px rgba(0,0,0,0.3)"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
