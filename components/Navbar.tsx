/* eslint-disable @next/next/no-img-element */
import getMockAvatar from "@helpers/getMockAvartar"
import { userState } from "@store"
import { getSession } from "next-auth/client"
import Link from "next/link"
import { useEffect } from "react"
import { useRecoilState } from "recoil"

export default function Navbar() {
	const [user, setUser] = useRecoilState(userState)

	useEffect(() => {
		if (user) return

		getSession().then((session) => {
			if (!session) return

			setUser(session.user)
		})
	}, [user, setUser])

	return (
		<div>
			<nav className="bg-yellow-2">
				<div className="max-w-7xl mx-auto  sm:px-6 lg:px-8">
					<div className="relative flex items-center justify-between h-16">
						<Link href="/">
							<a className="flex-1  flex items-center justify-center sm:items-stretch sm:justify-start cursor-pointer">
								<div className="flex-shrink-0 flex items-center ">
									<img
										className="block h-12 w-auto"
										src="/logo.svg"
										alt="Workflow"
									/>
									<h2 className="text-white ml-2">CookShare</h2>
								</div>
							</a>
						</Link>
						<div className="absolute inset-y-0 right-0 md:flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0 space-x-8 text-white hidden">
							<input
								type="text"
								className="w-80 px-3 py-1 text-center"
								placeholder="ค้นหาเมนูอาหาร"
							/>
							<Link href="/">
								<a>หน้าหลัก</a>
							</Link>
							{user ? (
								<>
									<Link href="/recipe/create">สร้างโพสต์</Link>
									<Link href="/#">แจ้งเตือน</Link>
									<Link href="/user">
										<a className="flex items-center space-x-2 cursor-pointer text-white">
											<img
												src={user.avatar || getMockAvatar(user.email!)}
												alt=""
												className="w-8 h-8 object-cover rounded-full shadow-md bg-white"
											/>
											<div>{user.username}</div>
										</a>
									</Link>
								</>
							) : (
								<div className="">
									<Link href="/sign_in">
										<a className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-yellow-2 hover:bg-white mt-4 lg:mt-0">
											เข้าสู่ระบบ/สมัครสมาชิก
										</a>
									</Link>
								</div>
							)}
						</div>
					</div>
				</div>

				<div className="sm:hidden" id="mobile-menu">
					<div className="px-2 pt-2 pb-3 space-y-3 flex flex-col items-center">
						<input
							type="text"
							className="w-80 px-3 py-1 text-center"
							placeholder="ค้นหาเมนูอาหาร"
						/>
						<Link href="/">
							<a>หน้าหลัก</a>
						</Link>
						{user ? (
							<>
								<Link href="/recipe/create">สร้างโพสต์</Link>
								<Link href="/#">แจ้งเตือน</Link>
								<div className="flex items-center space-x-2 cursor-pointer text-white">
									<img
										src={"https://randomuser.me/api/portraits/men/30.jpg"}
										alt=""
										className="w-8 rounded-full"
									/>
									<div>{user.username}</div>
								</div>
							</>
						) : (
							<div className="">
								<Link href="/sign_in">
									<a className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-yellow-2 hover:bg-white mt-4 lg:mt-0">
										เข้าสู่ระบบ/สมัครสมาชิก
									</a>
								</Link>
							</div>
						)}
					</div>
				</div>
			</nav>
		</div>
	)
}
