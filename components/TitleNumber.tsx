import { useEffect, useState } from "react"
interface Props{
    title:string|number
}
export default function TitleNumber(props:Props){
    const[title,setTitle]=useState<string|number>(1)
    useEffect(()=>{
       setTitle(props.title) 
    },[props.title])
    return <div className='bg-yellow-2 w-7 h-7 flex items-center justify-center rounded-full text-white'>
        {title}
    </div>
}