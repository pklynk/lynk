import { IRecipe, IUser } from "@type"
import axios from "@configs/axios"
import { FC, useEffect, useState } from "react"
import { useRecoilValue } from "recoil"
import { userState } from "@store"
import PostCard from "../PostCard"

const UserPosts: FC = () => {
	const user = useRecoilValue(userState)
	const [recipes, setRecipes] = useState<IRecipe[]>([])

	const fetchRecipe = async () => {
		const res = await axios().get("/api/v1/user/recipes?status=public")
		setRecipes(res.data.data)
	}

	useEffect(() => {}, [])

	useEffect(() => {
		fetchRecipe()
	}, [])

	return (
		<div className="grid grid-cols-9 gap-x-24">
			<div className="col-span-5 space-y-8">
				{recipes?.map((recipe) => (
					<PostCard key={recipe._id} recipe={recipe} user={user!} edit />
				))}
			</div>
			<div></div>
			<div></div>
			{/* <div
				className="col-span-3 justify-self-end bg-white w-full rounded-lg shadow-md px-8 py-14 space-y-12 h-full"
				style={{ height: "fit-content" }}
			>
				<div>
					<div className="text-2xl">อ่านมากที่สุด</div>
					<div className="ml-7 space-y-2">
						<div className="text-lg">ผัดกะเพราโบราณ</div>
						<div className="text-lg">ผัดกะเพราโบราณ</div>
						<div className="text-lg">ผัดกะเพราโบราณ</div>
					</div>
				</div>
				<div>
					<div className="text-2xl">ถูกใจมากที่สุด</div>
					<div className="ml-7 space-y-2">
						<div className="text-lg">ผัดกะเพราโบราณ</div>
					</div>
				</div>
			</div> */}
		</div>
	)
}

export default UserPosts
