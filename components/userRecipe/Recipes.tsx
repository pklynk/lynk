/* eslint-disable @next/next/no-img-element */
import { userState } from "@store"
import { IRecipe } from "@type"
import axios from "@configs/axios"
import { FC, useEffect, useState } from "react"
import { useRecoilValue } from "recoil"
import dateTimeFormat from "@helpers/dateTimeFormat"
import Link from "next/link"

const UserRecipes: FC = () => {
	const user = useRecoilValue(userState)
	const [recipes, setRecipes] = useState<IRecipe[]>([])

	const fetchRecipe = async () => {
		const res = await axios().get("/api/v1/user/recipes")

		setRecipes(res.data.data)
	}

	useEffect(() => {
		fetchRecipe()
	}, [user])
	return (
		<div className="grid grid-cols-3 gap-12">
			{recipes.map((recipe) => (
				<Link href={`/recipe/${recipe._id}`} key={recipe._id}>
					<a className="bg-white rounded-lg shadow-md overflow-hidden">
						<img
							className="h-64 w-full object-cover shadow-md rounded-lg"
							src={recipe.image! as string}
							alt=""
						/>
						<div className="p-4 space-y-3">
							<div className="text-xl">{recipe.title}</div>
							<div className="flex space-x-1 text-sm text-gray-500">
								<img src="/date.svg" alt="" />
								<div>{dateTimeFormat(recipe.createdAt || "")}</div>
							</div>
						</div>
					</a>
				</Link>
			))}
		</div>
	)
}

export default UserRecipes
