/* eslint-disable @next/next/no-img-element */
export default function AuthBox(props:any){
    return <div className="h-100 grid grid-cols-2 shadow rounded-xl bg-white">
        <div className="flex items-center justify-center h-full">
            {props.children}           
        </div>
        <div className="">
            <img className="h-100 w-full object-cover object-top rounded-r-xl" src="/chef.jpg" alt="" />
        </div>
    </div>
    }