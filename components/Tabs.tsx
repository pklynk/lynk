import { FC, useEffect, useState } from "react"
interface TabProps {
	title: string
	component: JSX.Element
}

interface Props {
	tabs: TabProps[]
}

const Tabs: FC<Props> = (props) => {
	const [currentIndex, setCurrentIndex] = useState<number>(0)
	const [tabs, setTabs] = useState<TabProps[]>([])

	useEffect(() => {
		setTabs(props.tabs)
	}, [props.tabs])

	return (
		<div className="space-y-8">
			<div className="bg-white w-full flex justify-between items-center text-xl px-24 py-4 shadow-md rounded-lg cursor-pointer">
				{tabs.map((tab, i) => (
					<div  onClick={() => setCurrentIndex(i)} key={i}
                    className={i === currentIndex ? "border-b-2 pb-1 border-yellow-1": ""}
                    >
						{tab.title}
					</div>
				))}
			</div>
			{tabs[currentIndex]?.component || <></>}
		</div>
	)
}

export default Tabs
