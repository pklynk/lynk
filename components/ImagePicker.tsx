/* eslint-disable @next/next/no-img-element */
import { onUploadToBlob } from "@helpers/upload"
import React, { ChangeEvent, useEffect, useState } from "react"

interface Props {
	onChange?: (file: File) => any
	previewImage?: string
}

export default function ImagePicker(props: Props) {
	const onChange = async (e: ChangeEvent<HTMLInputElement>) => {
		const file = e.target.files![0]
		if (!file) return
		setPreviewImage((await onUploadToBlob(file)) as string)
		if (typeof props.onChange === "function") {
			props.onChange(file)
		}
	}

	const [previewImage, setPreviewImage] = useState<string>("")

	useEffect(() => {
		setPreviewImage(props.previewImage || "")
	}, [props.previewImage])

	return (
		<div className="w-full h-full bg-gray-300 rounded-lg flex justify-center items-center relative relative">
			<input
				onChange={onChange}
				type="file"
				accept="image/png, image/jpeg"
				className="h-full w-full absolute top-0 right-0 opacity-0 cursor-pointer z-10"
			/>
			{previewImage ? (
				<>
					<img
						src={previewImage}
						className="w-full h-full absolute top-0 right-0 rounded-lg object-cover"
					/>
					<img
						src="/edit.svg"
						className="absolute top-5 right-5 z-20 cursor-pointer text-red-100"
					/>
				</>
			) : (
				<img className="w-14 h-14" src="/image.svg" />
			)}
		</div>
	)
}
