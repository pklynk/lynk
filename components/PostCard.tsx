/* eslint-disable @next/next/no-img-element */
import dateTimeFormat from "@helpers/dateTimeFormat"
import getMockAvatar from "@helpers/getMockAvartar"
import { IRecipe, IUser } from "@type"
import { FC, useEffect, useState } from "react"
import Link from "next/link"

interface Props {
	recipe: IRecipe
	user: IUser
	edit?: boolean
}

const PostCard: FC<Props> = (props) => {
	const [recipe, setRecipe] = useState<IRecipe>({})
	const [user, setUser] = useState<IUser>({})

	const [loading, setLoading] = useState<boolean>(true)

	useEffect(() => {
		if (!props.user || !props.recipe) return
		setRecipe(props.recipe)
		setUser(props.user)
		setLoading(false)
	}, [props.recipe, props.user])

	if (loading) return <></>

	return (
		<div
			key={recipe._id}
			className="bg-white rounded-lg shadow-md p-8 space-y-5"
		>
			<div className="flex justify-between">
				<div className="flex items-center space-x-8">
					<img
						src={user?.avatar || getMockAvatar(user?.email!)}
						alt=""
						className="w-24 h-24 cursor-pointer object-cover rounded-full shadow-md"
					/>
					<div>
						<div className="text-lg">
							{user?.firstName} {user?.lastName}
						</div>
						<div className="text-sm text-gray-500">{user?.username}</div>
						<div className="flex items-center space-x-1 pt-2">
							<img src="/date.svg" alt="" />
							<div className="text-sm text-gray-500">
								{dateTimeFormat(recipe.createdAt || "")}
							</div>
						</div>
					</div>
				</div>
				<div>
					<div className="flex items-center space-x-3 cursor-pointer">
						{props.edit && (
							<Link href={`/recipe/${recipe._id}/edit`}>
								<a>
									<img src="/setting.svg" alt="" className="w-6" />
								</a>
							</Link>
						)}

						<Link href={`/recipe/${recipe._id}`}>
							<a>
								<img src="/link.svg" alt="" />
							</a>
						</Link>
					</div>
				</div>
			</div>
			<div className="space-y-4">
				<div className="text-xl">{recipe.title}</div>
				<img
					src={recipe.image as string}
					alt=""
					className="w-full h-80 object-cover rounded-lg"
				/>
			</div>
			<div className="flex justify-between">
				<div className="flex space-x-3">
					<img src="/heart-black.svg" alt="" />
					<div>{recipe.likeCounts}</div>
				</div>
				<div className="flex space-x-3">
					<img src="/comment.svg" alt="" />
					<div>{recipe.comments?.length}</div>
				</div>
				<div className="flex space-x-3">
					<img src="/eye.svg" alt="" />
					<div>{recipe.rating?.views}</div>
				</div>
			</div>
		</div>
	)
}

export default PostCard
