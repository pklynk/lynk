/* eslint-disable @next/next/no-img-element */
import React from "react"
import CreateIngredientForm from "./CreateIngredientForm"
import { useRecoilState } from "recoil"
import { createIngredientStates } from "@store"

interface Props {
	onRemove?: () => any
	onChange?: (form: any) => any
}

export default function CreateIngredients(props: Props) {
	const [ingredients, setIngredients] = useRecoilState(createIngredientStates)

	const add = () => {
		setIngredients((prev) => [...prev, {}])
	}

	return (
		<>
			{ingredients.map((_, i) => (
				<CreateIngredientForm key={ingredients.length + i} index={i} />
			))}
			<div
				onClick={add}
				className="bg-yellow-2 rounded-md shadow-md flex space-x-5 w-40 justify-center items-center px-3 py-2 cursor-pointer"
			>
				<img className="w-7 h-7" src="/add.svg" />
				<div className="text-white">เพิ่มวัตถุดิบ</div>
			</div>
		</>
	)
}
