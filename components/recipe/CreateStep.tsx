/* eslint-disable @next/next/no-img-element */
import { createStepStates } from "@store"
import { useRecoilState } from "recoil"
import CreateStepForm from "./CreateStepForm"

export default function CreateStep() {
	const [steps, setSteps] = useRecoilState(createStepStates)

	const add = () => {
		setSteps((prev) => [...prev, {}])
	}

	return (
		<>
			{steps.map((_, i) => (
				<CreateStepForm key={steps.length + i} index={i} />
			))}

			<div
				onClick={add}
				className="bg-yellow-2 rounded-md shadow-md flex space-x-5 w-40 justify-center items-center px-3 py-2 cursor-pointer"
			>
				<img className="w-7 h-7" src="/add.svg" />
				<div className="text-white">เพิ่มวิธีทำ</div>
			</div>
		</>
	)
}
