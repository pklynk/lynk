/* eslint-disable @next/next/no-img-element */
import dateTimeFormat from "@helpers/dateTimeFormat"
import { IComment } from "@type"
import axios from "@configs/axios"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import getMockAvatar from "@helpers/getMockAvartar"
import { useRecoilValue } from "recoil"
import { userState } from "@store"
interface Props {
	comments?: IComment[]
	onRefetch?: () => any
}
export default function Comment(props: Props) {
	const [comments, setComments] = useState<IComment[]>([])
	const [comment, setComment] = useState<string>("")
	const router = useRouter()

	const user = useRecoilValue(userState)

	useEffect(() => {
		setComments(props.comments || [])
	}, [props.comments])

	const onSubmit = async () => {
		await axios().post(`/api/v1/recipes/comment?id=${router.query.id}`, {
			text: comment,
		})
		setComment("")
		if (typeof props.onRefetch === "function") {
			props.onRefetch()
		}
	}

	const onDelete = async (c: any) => {
		await axios().delete(`/api/v1/recipes/comment/${c._id}`)
		if (typeof props.onRefetch === "function") {
			props.onRefetch()
		}
	}

	return (
		<div className="space-y-5">
			<div className="space-y-3">
				<div className="text-2xl">แสดงความคิดเห็น</div>
				<div className="relative w-full h-44 rounded-lg border-2 border-gray-300 p-3 focus:outline-none">
					<textarea
						value={comment}
						onChange={(e) => setComment(e.target.value)}
						className="w-full h-full focus:outline-none resize-none"
					></textarea>
					<div
						onClick={onSubmit}
						className="flex absolute bottom-3 right-3 space-x-3 items-center cursor-pointer"
					>
						<div className="text-yellow-2 text-xl">ส่ง</div>
						<img className="w-6 h-6" src="/send.svg" />
					</div>
				</div>
			</div>

			<div className="flex items-center space-x-3">
				<img src="/comment.svg" />
				<div>แสดงความคิดเห็น ( {comments.length} ) </div>
			</div>
			<div className="space-y-12">
				{comments.map((comment, i) => (
					<div key={i}>
						<div className="flex items-center space-x-5">
							<img
								className="w-16 h-16 rounded-full object-cover"
								src={
									comment?.user?.avatar || getMockAvatar(comment.user?.email)
								}
							/>
							<div>
								<div>
									{comment?.user?.firstName} {comment?.user?.lastName}
								</div>
								<div className="text-gray-500 text-sm">
									{comment?.user?.username}
								</div>
							</div>
						</div>
						<div className="ml-20 space-y-5">
							<div>{comment?.text}</div>
							<div className="flex justify-between">
								<div className="text-gray-500 text-sm">
									{dateTimeFormat(comment?.createdAt || "")} น.
								</div>
								{comment?.user?._id === user?._id && (
									<img
										onClick={() => onDelete(comment)}
										src="/delete.svg"
										className="cursor-pointer"
									/>
								)}
							</div>
						</div>
					</div>
				))}
			</div>
		</div>
	)
}
