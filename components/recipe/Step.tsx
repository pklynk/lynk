/* eslint-disable @next/next/no-img-element */
import { useEffect, useState } from "react"
import TitleNumber from "@components/TitleNumber"
import { IStep } from "@type"
interface Props {
	steps?: IStep[]
}
export default function Step(props: Props) {
	const [steps, setSteps] = useState<IStep[]>([])
	useEffect(() => {
		setSteps(props.steps || [])
	}, [props.steps])
	return (
		<div className="space-y-5">
			<div className="font-bold text-xl">ขั้นตอน</div>
			{steps.map((step, i) => (
				<div key={i} className="space-y-7">
					<div className="flex space-x-5 ">
						<div>
							<TitleNumber title={i + 1}></TitleNumber>
						</div>

						<div className="">{step.text}</div>
					</div>
					<div className="grid grid-cols-3 gap-12 ml-11">
						{step?.images?.map((image, idx) => (
							<img
								key={idx}
								src={image}
								className="w-full h-40 object-cover rounded-lg"
							/>
						))}
					</div>
				</div>
			))}
		</div>
	)
}
