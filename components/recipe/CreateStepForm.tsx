/* eslint-disable @next/next/no-img-element */
import TitleNumber from "@components/TitleNumber"
import { createStepStates } from "@store"
import { IStep } from "@type"
import React, { FC, useEffect, useState } from "react"
import { useRecoilState } from "recoil"
interface Props {
	index: number
}
const CreateStepForm: FC<Props> = (props) => {
	const defaultValue: IStep = {
		images: [],
		text: "",
	}

	const [form, setForm] = useState<IStep>({})
	const [createSteps, setCreateSteps] = useRecoilState(createStepStates)

	useEffect(() => {
		const clone: IStep[] = JSON.parse(JSON.stringify(createSteps))
		clone[props.index] = form
		setCreateSteps(clone)
	}, [form])

	useEffect(() => {
		const step = createSteps[props.index]

		if (!Object.values(step).length) {
			setForm(defaultValue)
		} else {
			setForm(step)
		}
	}, [props.index])

	const onRemove = () => {
		const clone: IStep[] = JSON.parse(JSON.stringify(createSteps))
		clone.splice(props.index, 1)
		setCreateSteps(clone)
	}

	return (
		<div className="space-y-5">
			<div className="flex justify-between items-center">
				<TitleNumber title={props.index + 1}></TitleNumber>
				<div
					onClick={onRemove}
					className="bg-red-1 h-10 rounded-md shadow-md self-end flex justify-center items-center w-16 justify-self-end cursor-pointer"
				>
					<img className="w-7 h-7" src="/minus.svg" />
				</div>
			</div>
			<textarea
				value={form.text}
				onChange={(e) => setForm((prev) => ({ ...prev, text: e.target.value }))}
				className="w-full h-60 rounded-md shadow-md focus:outline-none resize-none px-8 py-4"
			></textarea>
			{/* <div className="flex justify-between ">
				<div className="h-32 w-44">
					<ImagePicker></ImagePicker>
				</div>
				<div className="h-32 w-44">
					<ImagePicker></ImagePicker>
				</div>
				<div className="h-32 w-44">
					<ImagePicker></ImagePicker>
				</div>
				<div className="h-32 w-44">
					<ImagePicker></ImagePicker>
				 </div>
			</div> */}
		</div>
	)
}

export default CreateStepForm
