import { IIngredient } from "@type"
import { useEffect, useState } from "react"
interface Props {
	ingredients?: IIngredient[]
}
export default function Ingredient(props: Props) {
	const [ingredients, setIngredients] = useState<IIngredient[]>([])
	useEffect(() => {
		setIngredients(props.ingredients || [])
	}, [props.ingredients])
	return (
		<div>
			<div className="font-bold text-xl">วัตถุดิบ</div>
			{ingredients.map((ingredient, i) => (
				<div
					key={i}
					className={
						i % 2 !== 0 ? "bg-gray-200 px-3 py-1" : "px-3 py-1 bg-white"
					}
				>
					{ingredient.name} {ingredient.amount} {ingredient.unit}
				</div>
			))}
		</div>
	)
}
