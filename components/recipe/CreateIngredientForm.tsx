/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect } from "react"
import { IIngredient } from "@type"
import { createIngredientStates } from "@store"
import { useRecoilState } from "recoil"

interface Props {
	index: number
}

const CreateIngredientForm: React.FC<Props> = ({ index }) => {
	const defaultValue: IIngredient = {
		name: "",
		unit: "",
		amount: 1,
		image: "",
	}
	const [form, setForm] = useState<IIngredient>(defaultValue)
	const [createIngredients, setCreateIngredients] = useRecoilState(
		createIngredientStates
	)

	useEffect(() => {
		const clone: IIngredient[] = JSON.parse(JSON.stringify(createIngredients))
		clone[index] = form
		setCreateIngredients(clone)
	}, [form])

	useEffect(() => {
		const ingrendient = createIngredients[index]
		if (!Object.values(ingrendient).length) {
			setForm(defaultValue)
		} else {
			setForm(ingrendient)
		}
	}, [index])

	const onRemove = () => {
		const clone: IIngredient[] = JSON.parse(JSON.stringify(createIngredients))
		clone.splice(index, 1)
		setCreateIngredients(clone)
	}

	return (
		<>
			<div className="space-y-5">
				<div className="grid grid-cols-12 gap-x-7">
					<div className="space-y-2 col-span-9">
						<div>ชื่อวัตถุดิบ</div>
						<input
							value={form.name}
							onChange={(e) =>
								setForm((prev) => ({ ...prev, name: e.target.value }))
							}
							type="text"
							className="w-full focus:outline-none shadow-md rounded-md px-4 py-2"
						/>
					</div>
					<div className="space-y-2">
						<div>ปริมาณ</div>
						<input
							value={form.amount}
							onChange={(e) =>
								setForm((prev) => ({ ...prev, amount: ~~e.target.value }))
							}
							type="number"
							min="1"
							className="w-full focus:outline-none shadow-md rounded-md px-4 py-2"
						/>
					</div>
					<div className="space-y-2">
						<div>หน่วย</div>
						<input
							value={form.unit}
							onChange={(e) =>
								setForm((prev) => ({ ...prev, unit: e.target.value }))
							}
							type="text"
							className="w-full focus:outline-none shadow-md rounded-md px-4 py-2"
						/>
					</div>

					<div
						onClick={onRemove}
						className="bg-red-1 h-10 rounded-md shadow-md self-end flex justify-center items-center w-16 justify-self-end cursor-pointer"
					>
						<img className="w-7 h-7" src="/minus.svg" />
					</div>
				</div>
				{/* <div className="h-32 w-44">
					<ImagePicker
						onChange={(file) => setForm((prev) => ({ ...prev, file }))}
					></ImagePicker>
				</div> */}
			</div>
		</>
	)
}

export default CreateIngredientForm
