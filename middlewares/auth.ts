import { GetServerSideProps, GetServerSidePropsContext } from "next"
import { getSession } from "next-auth/client"

export default function auth(
	handler: (context: GetServerSidePropsContext) => any
): GetServerSideProps {
	return async (context: GetServerSidePropsContext) => {
		const session = await getSession({ req: context.req })
		if (!session?.token) {
			context.res.writeHead(302, {
				Location: "/sign_in",
			})
			context.res.end()
			return
		}
		context.locale = JSON.stringify({ session })
		return handler(context)
	}
}
