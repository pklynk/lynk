import axios, { AxiosInstance } from "axios"
import { getSession } from "next-auth/client"

const _axios = axios.create()

function instance(req?: any): AxiosInstance {
	_axios.interceptors.request.use(async (configs) => {
		if (process.browser) {
			configs.baseURL =
				process.env.NEXT_PUBLIC_API_BASE_URL || "http://localhost:5000"
		} else {
			configs.baseURL =
				process.env.NEXT_PUBLIC_API_BASE_URL_SERVER_SIDE || "http://localhost:5000"
		}
		const session = await getSession({ req })
		if (session?.token) {
			configs.headers.authorization = `Bearer ${session.token}`
		}
		return configs
	})
	return _axios
}

export default instance
