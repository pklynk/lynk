import Navbar from "../components/Navbar"
import AuthBox from "../components/AuthBox"
import Link from "next/link"
import React, { useState } from "react"
import axios from "@configs/axios"
import { useRouter } from "next/router"

export default function SignUp() {
	const [firstName, setFirstName] = useState<string>("")
	const [lastname, setLastName] = useState<string>("")
	const [email, setEmail] = useState<string>("")
	const [username, setUsername] = useState<string>("")
	const [password, setPassword] = useState<string>("")
	const [confirmPassword, setComfirmPassword] = useState<string>("")

	const router = useRouter()

	const submit = async (e: any) => {
		e.preventDefault()

		try {
			await axios().post("/api/v1/auth/sign_up", {
				firstName,
				lastname,
				username,
        email,
				password,
				confirmPassword,
			})
			router.push("/sign_in")
		} catch (error) {
			window.alert("สมัครสมาชิกไม่สำเร็จ")
		}
	}

	return (
		<div className="h-screen">
			<div className="absolute w-full">
				<Navbar></Navbar>
			</div>
			<div className="h-screen bg-grey-2 pt-7 flex items-center justify-center">
				<AuthBox>
					<form onSubmit={submit} className="flex flex-col space-y-3 w-2/3">
						<div className="flex space-x-5">
							<input
								required
								onChange={(e) => setFirstName(e.target.value)}
								value={firstName}
								className="border-2 border-black rounded px-3 py-1 w-full"
								placeholder="ชื่อ"
							/>
							<input
								required
								onChange={(e) => setLastName(e.target.value)}
								value={lastname}
								className="border-2 border-black rounded px-3 py-1 w-full"
								placeholder="นามสกุล"
							/>
						</div>
						<input
							required
							onChange={(e) => setEmail(e.target.value)}
							value={email}
							className="border-2 border-black rounded px-3 py-1"
							placeholder="อีเมล"
						/>
						<input
							required
							onChange={(e) => setUsername(e.target.value)}
							value={username}
							className="border-2 border-black rounded px-3 py-1"
							placeholder="ชื่อผู้ใช้"
						/>
						<input
							required
							onChange={(e) => setPassword(e.target.value)}
							value={password}
							type="password"
							className="border-2 border-black rounded px-3 py-1"
							placeholder="รหัสผ่าน"
						/>
						<input
							required
							onChange={(e) => setComfirmPassword(e.target.value)}
							value={confirmPassword}
							type="password"
							className="border-2 border-black rounded px-3 py-1"
							placeholder="ยืนยันรหัสผ่าน"
						/>
						<input
							onClick={submit}
							value={"ลงทะเบียน"}
							type="submit"
							className="bg-yellow-2 flex items-center justify-center text-white py-1 rounded cursor-pointer"
						/>
						<div>
							<span className="mr-1 ">คุณมีบัญชีอยู่แล้ว?</span>
							<Link href="/sign_in">
								<a className="underline pb-2">เข้าสู่ระบบ</a>
							</Link>
						</div>
					</form>
				</AuthBox>
			</div>
		</div>
	)
}
