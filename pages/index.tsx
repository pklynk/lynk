/* eslint-disable @next/next/no-img-element */
import axios from "@configs/axios"
import { IRecipe } from "@type"
import { GetServerSideProps } from "next"
import React, { FC } from "react"
import Navbar from "../components/Navbar"
import Link from "next/link"

interface Props {
	recipes: {
		recent: IRecipe[]
		popular: IRecipe[]
		suggest: IRecipe[]
	}
}

const RecipeCard: FC<{ recipe: IRecipe }> = ({ recipe }) => {
	return (
		<Link href={`/recipe/${recipe._id}`}>
			<a className="group relative shadow-lg rounded-lg px-2 py-2 bg-white">
				<div className="w-full shadow-md min-h-80 bg-gray-200 aspect-w-1 aspect-h-1 rounded-md overflow-hidden group-hover:opacity-75 lg:h-80 lg:aspect-none">
					<img
						className="object-cover object-center h-full w-full"
						src={recipe.image! as string}
						alt={`${recipe.title}`}
					/>
				</div>
				<div className="mt-4 bg-white">
					<div className="">
						<h3 className="text-base Roboto text-gray-700 font-bold">
							<div>
								<span aria-hidden="true" className="absolute inset-0"></span>
								{recipe.title}
							</div>
						</h3>
						<p className="mt-1 text-xs Roboto text-gray-500">
							{recipe.user?.firstName} {recipe.user?.lastName}
						</p>

						<div className="text-sm text-right text-gray-500 space-x-4">
							<span>{recipe.likeCounts} likes</span>
							<span>{recipe.rating?.views} views</span>
						</div>
					</div>
				</div>
			</a>
		</Link>
	)
}

export default function Home(props: Props) {
	const suggestLabels: { title: string }[] = [
		{ title: "อาหารคีโต" },
		{ title: "อาหารตามสั่ง" },
		{ title: "เมนูกุ้ง" },
		{ title: "ของหวาน" },
		{ title: "คั่วกลิ้ง" },
		{ title: "อาหารจานเดียว" },
	]
	return (
		<div className="bg-grey-2 bg-opacity-50 shadow-sm pb-12">
			<div className="absolute w-full">
				<Navbar></Navbar>
			</div>

			<div className="px-14 pt-28 pb-6 ">
				<img
					className="w-full h-96 object-cover shadow rounded-md "
					src="https://image.makewebeasy.net/makeweb/0/1Sya49BjZ/Food/2019_12_06_S_P_S09_0627.jpg"
					alt=""
				/>
			</div>

			<div className="mt-1 flex justify-between  sm:flex-row  sm:flex-wrap sm:mt-0 sm:space-x-6">
				<div className="mt-2 flex  pl-14 Roboto text-lg font-semibold text-gray-800">
					เมนูฮอตฮิตวันนี้
				</div>
				<div className="mt-2 flex pr-14 Roboto text-lg font-semibold hover:underline text-gray-800 ">
					เพิ่มเติม
				</div>
			</div>

			<div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8 px-20">
				{props.recipes.popular.map((recipe) => (
					<RecipeCard recipe={recipe} key={recipe._id} />
				))}
			</div>

			<div className="mt-1 flex justify-between  sm:flex-row  sm:flex-wrap sm:mt-0 sm:space-x-6">
				<div className="mt-2 flex  pl-14 Roboto text-lg font-semibold text-gray-800">
					เมนูแนะนำ
				</div>
				<div className="mt-2 flex pr-14 Roboto text-lg font-semibold hover:underline text-gray-800 ">
					เพิ่มเติม
				</div>
			</div>

			<div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8 px-20">
				{props.recipes.suggest.map((recipe) => (
					<RecipeCard recipe={recipe} key={recipe._id} />
				))}
			</div>

			<div className="mt-1 flex justify-between  sm:flex-row  sm:flex-wrap sm:mt-0 sm:space-x-6">
				<div className="mt-2 flex  pl-14 Roboto text-lg font-semibold text-gray-800">
					เมนูล่าสุด
				</div>
				<div className="mt-2 flex pr-14 Roboto text-lg font-semibold hover:underline text-gray-800 ">
					เพิ่มเติม
				</div>
			</div>

			<div className="mt-6 grid grid-cols-auto gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8 px-20">
				{props.recipes.recent.map((recipe) => (
					<RecipeCard recipe={recipe} key={recipe._id} />
				))}
			</div>

			<div className=" px-20 mt-4 mb-4">
				<button
					type="submit"
					className="group relative w-full flex justify-center py-2 px-4 bg-yellow-300 text-yellow-900 shadow-lg  hover:bg-yellow-500 hover:text-yellow-100 font-bold  rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
				>
					<span className="absolute left-0 inset-y-0 flex items-center pl-3"></span>
					เพิ่มเติม
				</button>
			</div>

			<div className="mt-2 flex  pl-20 Roboto text-lg font-semibold text-gray-800">
				การค้นหาที่น่าสนใจ
			</div>

			<div className="grid grid-cols-6 divide-x divide-gray-400  px-20 ">
				{suggestLabels.map((label, i) => (
					<div
						key={i}
						className="flex-1 bg-opacity-25 Roboto text-gray-600 text-center rounded-md hover:underline bg-gray-400 px-4 py-2 m-2"
					>
						{label.title}
					</div>
				))}
			</div>
		</div>
	)
}

export const getServerSideProps: GetServerSideProps = async ({ req }) => {
	const baseUrl = "/api/v1/guest"

	const [popular, suggest, recent]: IRecipe[][] = await Promise.all([
		axios()
			.get(`${baseUrl}/popular?limit=4`)
			.then((res) => res.data.data || []),
		axios()
			.get(`${baseUrl}/suggest?limit=4`)
			.then((res) => res.data.data || []),
		axios()
			.get(`${baseUrl}/recent?limit=8`)
			.then((res) => res.data.data || []),
	])

	return {
		props: {
			recipes: {
				recent,
				popular,
				suggest,
			},
		},
	}
}
