/* eslint-disable @next/next/no-img-element */
import Navbar from "../components/Navbar";
import AuthBox from "../components/AuthBox";


export default function ForgetPassword() {
  const submit = async (e: any) => {
    e.preventDefault();
  };
  return (
    <div className="">
      <div className="absolute w-full">
        <Navbar></Navbar>
      </div>
      <div className="h-screen bg-grey-2 pt-7 flex items-center justify-center">
        <AuthBox>
          <div>
            <div>คุณต้องการรับรหัสเพื่อรีเซ็ตด้วยวิธีใด</div>
            <div className="flex space-x-3">
              <img src="/Check.svg" alt="" />

              <div className="flex flex-col">
                <span>ส่งรหัสไปทางอีเมล</span>
                <span> ******@gmail.com</span>
              </div>
            </div>
            <div className='flex justify-center'>
            <div className="bg-yellow-2 flex items-center justify-center text-white py-1 rounded mt-5 w-36">ดำเนินการต่อ</div>
            </div>
          </div>
        </AuthBox>
      </div>
    </div>
  );
}
