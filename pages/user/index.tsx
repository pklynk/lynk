/* eslint-disable @next/next/no-img-element */
import Navbar from "@components/Navbar"
import Tabs from "@components/Tabs"
import UserPosts from "@components/userRecipe/Posts"
import UserRecipes from "@components/userRecipe/Recipes"
import getMockAvatar from "@helpers/getMockAvartar"
import auth from "@middlewares/auth"
import { userState } from "@store"
import { ISession } from "@type"
import { signOut } from "next-auth/client"
import router from "next/router"
import { FC, useEffect } from "react"
import { useRecoilState } from "recoil"

interface Props {
	session: ISession
}
const UserProfile: FC<Props> = (props) => {
	const { user } = props.session
	const [, setUser] = useRecoilState(userState)

	useEffect(() => {
		setUser(user)
	}, [user, setUser])

	const onSignOut = () => {
		signOut()
		setUser({})
		router.replace("/")
	}

	return (
		<div>
			<Navbar />
			<div className="px-28 py-14 bg-grey-2 min-h-screen space-y-8">
				<div className="flex shadow-md px-8 py-4 rounded-lg bg-white">
					<img
						src={user?.avatar || getMockAvatar(user?.email)}
						className="w-44 h-44 object-cover rounded-full shadow-md"
						alt=""
					/>
					<div className="flex flex-col justify-between px-14 w-full">
						<div className="flex justify-between">
							<div>
								<div className="text-3xl">
									{user?.firstName} {user?.lastName}
								</div>
								<div className="text-gray-500">{user?.username}</div>
							</div>
							<div className="flex items-center space-x-3">
								<div className="cursor-pointer">
									<img src="/setting.svg" alt="" />
								</div>
								<div
									onClick={onSignOut}
									className="cursor-pointer bg-red-1 rounded-lg p-2 shadow-md"
								>
									<img src="/logout.svg" alt="" className="w-5 h-5" />
								</div>
							</div>
						</div>
						<div className="flex items-baseline justify-between w-full text-xl">
							<div className="space-y-3">
								<div className="text-center text-2xl">1329</div>
								<div>ยอดเข้าชมทั้งหมด</div>
							</div>
							<div className="space-y-3">
								<div className="text-center text-2xl">12</div>
								<div>โพสต์</div>
							</div>
							<div className="space-y-3">
								<div className="text-center text-2xl">29</div>
								<div>ผู้ติดตาม</div>
							</div>
							<div className="space-y-3">
								<div className="text-center text-2xl">5</div>
								<div>กำลังติดตาม</div>
							</div>
						</div>
					</div>
				</div>
				<Tabs
					tabs={[
						{ title: "โพสต์", component: <UserPosts /> },
						{ title: "สูตรอาหารของฉัน", component: <UserRecipes /> },
					]}
				/>
			</div>
		</div>
	)
}

export const getServerSideProps = auth(({ locale }) => {
	const { session } = JSON.parse(locale!)
	return {
		props: {
			session,
		},
	}
})

export default UserProfile
