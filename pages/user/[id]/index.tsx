/* eslint-disable @next/next/no-img-element */
import Navbar from "@components/Navbar"
import { IRecipe, IUser } from "@type"
import axios from "@configs/axios"
import { useRouter } from "next/router"
import React, { FC, useEffect, useState } from "react"
import getMockAvatar from "@helpers/getMockAvartar"
import PostCard from "@components/PostCard"

const UserById: FC = () => {
	const router = useRouter()
	const [user, setUser] = useState<IUser>({})
	const [recipes, setRecipes] = useState<IRecipe[]>([])

	const fetchUser = async () => {
		const res = await axios().get(`/api/v1/users/${router.query.id}`)
		setUser(res.data)
	}

	const fetchRecipes = async () => {
		const res = await axios().get(`/api/v1/users/${router.query.id}/recipes?limit=20`)
		if (res.data.data) {
			setRecipes(res.data.data)
		}
	}

	useEffect(() => {
		if (!router.query.id) return
		fetchUser()
		fetchRecipes()
	}, [router.query.id])

	return (
		<div>
			<Navbar />
			<div className="px-28 py-14 bg-grey-2 min-h-screen space-y-8">
				<div className="flex shadow-md px-8 py-4 rounded-lg bg-white">
					<img
						src={user?.avatar || getMockAvatar(user?.email)}
						className="w-44 h-44 object-cover rounded-full shadow-md"
						alt=""
					/>
					<div className="flex flex-col justify-between px-14 w-full">
						<div className="flex justify-between">
							<div>
								<div className="text-3xl">
									{user?.firstName} {user?.lastName}
								</div>
								<div className="text-gray-500">{user?.username}</div>
							</div>
						</div>
						<div className="flex items-baseline justify-between w-full text-xl">
							<div className="space-y-3">
								<div className="text-center text-2xl">1329</div>
								<div>ยอดเข้าชมทั้งหมด</div>
							</div>
							<div className="space-y-3">
								<div className="text-center text-2xl">12</div>
								<div>โพสต์</div>
							</div>
							<div className="space-y-3">
								<div className="text-center text-2xl">29</div>
								<div>ผู้ติดตาม</div>
							</div>
							<div className="space-y-3">
								<div className="text-center text-2xl">5</div>
								<div>กำลังติดตาม</div>
							</div>
						</div>
					</div>
				</div>

				<div className="w-1/2 space-y-8 recipe-list">
					{recipes.map((recipe) => (
						<div key={recipe._id} className="recipe">
							<PostCard recipe={recipe} user={user!} />
						</div>
					))}
				</div>
			</div>
		</div>
	)
}

export default UserById
