import Navbar from "../components/Navbar"
import AuthBox from "../components/AuthBox"
import Link from "next/link"
import React, { useState } from "react"
import { signIn, getSession } from "next-auth/client"
import { useRecoilState } from "recoil"
import { userState } from "@store"
import { useRouter } from "next/router"
export default function SignIn() {
	const [email, setEmail] = useState<string>("")
	const [password, setPassword] = useState<string>("")
	const [_, setUser] = useRecoilState(userState)
	const router = useRouter()

	const submit = async (e: any) => {
		e.preventDefault()
		try {
			const res = await signIn("credentials", {
				email,
				password,
				redirect: false,
			})
			if (!res?.ok) {
				throw new Error(res?.error)
			}

			const session = await getSession()
			setUser(session!.user!)

			router.push("/")
		} catch (error) {
			window.alert("ลงชื่อเข้าใช้ไม่สำเร็จ")
		}
	}
	return (
		<div className="">
			<div className="absolute w-full">
				<Navbar></Navbar>
			</div>
			<div className="h-screen bg-grey-2 pt-7 flex items-center justify-center">
				<AuthBox>
					<form onSubmit={submit} className="flex flex-col space-y-3">
						<input
							required
							onChange={(e) => setEmail(e.target.value)}
							value={email}
							className="border-2 border-black rounded px-3 py-1"
							placeholder="อีเมล"
						/>
						<input
							required
							onChange={(e) => setPassword(e.target.value)}
							value={password}
							type="password"
							className="border-2 border-black rounded px-3 py-1"
							placeholder="รหัสผ่าน"
						/>
						<div className="flex justify-end">
							<Link href="/forget_password">ลืมรหัสผ่าน</Link>
						</div>
						<input
							onClick={submit}
							value={"ลงชื่อเข้าใช้"}
							type="submit"
							className="bg-yellow-2 flex items-center justify-center text-white py-1 rounded cursor-pointer"
						/>
						<div>
							<span className="mr-1 ">คุณยังไม่มีบัญชีหรือไม่ ?</span>
							<Link href="/sign_up">
								<a className="underline pb-2">ลงทะเบียนตอนนี้เลย</a>
							</Link>
						</div>
					</form>
				</AuthBox>
			</div>
		</div>
	)
}
