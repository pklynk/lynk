/* eslint-disable @next/next/no-img-element */
import Navbar from "@components/Navbar"
import { useCallback, useEffect, useState } from "react"
import ImagePicker from "@components/ImagePicker"
import CreateIngredients from "@components/recipe/CreateIngredients"
import CreateStep from "@components/recipe/CreateStep"
import auth from "../../middlewares/auth"
import axios from "@configs/axios"
import { useRecoilState } from "recoil"
import { createIngredientStates, createStepStates } from "@store"
import { onUploadToStorage } from "@helpers/upload"
import { IRecipe } from "@type"
import Link from "next/link"
import { useRouter } from "next/router"

interface IDurationUnit {
	value: string
	text: string
}
interface IStatus {
	value: string
	text: string
	image: string
}

interface IRecipeForm {
	title: string
	amount: number
	status: string
	image: string | File
	duration: number
}

interface Props {
	query: {
		ref?: string
	}
}

export default function CreateRecipe(props: Props) {
	const status: IStatus[] = [
		{ value: "public", text: "สาธารณะ", image: "/earth.svg" },
		{ value: "private", text: "ส่วนตัว", image: "/lock.svg" },
	]

	const [form, setForm] = useState<IRecipeForm>({
		title: "",
		amount: 1,
		status: status[0].value,
		image: "",
		duration: 1,
	})

	const [ingredients, setIngredients] = useRecoilState(createIngredientStates)
	const [steps, setSteps] = useRecoilState(createStepStates)
	const [recipe, setRecipe] = useState<IRecipe>({})

	const router = useRouter()

	const getStatus = (value: string) => status.find((e) => e.value === value)

	const onSubmit = async () => {
		if (!form.image) return alert("กรุณาอัพโหลดรูปเมนูอาหาร")

		const imgUrl = await onUploadToStorage(form.image as File)

		const data: IRecipe = {
			...form,
			public: form.status === "public",
			image: imgUrl,
			steps,
			ingredients,
			refer: props.query.ref,
		}

		await axios().post("/api/v1/user/recipes", data)

		setForm({
			title: "",
			amount: 1,
			status: status[0].value,
			image: "",
			duration: 1,
		})

		setIngredients([])
		setSteps([])
		return () => {
			setIngredients([])
			setSteps([])
		}
	}

	const fetchRecipe = async () => {
		try {
			const res = await axios().get(`/api/v1/recipes/${props.query.ref}`)
			setRecipe(res.data.recipe)
			setForm({
				title: res.data.recipe.title!,
				amount: res.data.recipe.amount!,
				status: res.data.recipe.public ? "public" : "private",
				duration: res.data.recipe.duration!,
				image: res.data.recipe.image!,
			})
			setIngredients(res.data.recipe.ingredients)
			setSteps(res.data.recipe.steps)
		} catch (error) {
			router.replace("/404")
		}
	}

	useEffect(() => {
		if (props.query.ref) fetchRecipe()
	}, [])

	return (
		<div>
			<Navbar></Navbar>

			<div className="px-36 bg-grey-2 min-h-screen py-20 space-y-9">
				{props.query.ref && (
					<div className="flex justify-center">
						<div className="text-xl">
							อ้างอิงจากสูตร
							<Link href={`/recipe/${recipe._id}`}>
								<a className="text-yellow-1 px-2 underline">{recipe.title}</a>
							</Link>
						</div>
					</div>
				)}

				<div className="space-y-12">
					<div className="h-96">
						<ImagePicker
							onChange={(file) => setForm((prev) => ({ ...prev, image: file }))}
						></ImagePicker>
					</div>
					<div className="space-y-2">
						<div>ชื่อเมนูอาหาร</div>
						<input
							value={form.title}
							onChange={(e) =>
								setForm((prev) => ({ ...prev, title: e.target.value }))
							}
							type="text"
							className="w-full focus:outline-none shadow-md rounded-md px-4 py-2"
						/>
					</div>
					<div className="flex justify-between">
						<div className="space-y-2">
							<div>จำนวนเสริฟ</div>
							<input
								value={form.amount}
								onChange={(e) =>
									setForm((prev) => ({ ...prev, amount: ~~e.target.value }))
								}
								type="number"
								min="1"
								className="w-24 focus:outline-none shadow-md rounded-md px-4 py-2"
							/>
						</div>
						<div className="flex space-x-7 items-end">
							<div className="space-y-2">
								<div>ระยะเวลา</div>
								<input
									value={form.duration}
									onChange={(e) =>
										setForm((prev) => ({ ...prev, duration: ~~e.target.value }))
									}
									type="number"
									min="1"
									className="w-24 focus:outline-none shadow-md rounded-md px-4 py-2"
								/>
							</div>
							<div className="py-2">นาที</div>
						</div>
					</div>
					<div className="space-y-5">
						<div>วัตถุดิบ</div>
						<CreateIngredients></CreateIngredients>
					</div>
					<div className="space-y-5">
						<div>วิธีทำ</div>
						<CreateStep></CreateStep>
					</div>
					<div className="bg-yellow-2  flex items-center justify-between w-40 rounded-md px-3 py-2 shadow-md relative">
						<select
							onChange={(e) => {
								setForm((prev) => ({ ...prev, status: e.target.value }))
							}}
							className="w-full h-full absolute top-0 right-0 opacity-0"
						>
							{status.map((s, i) => (
								<option key={i} value={s.value}>
									{s.text}
								</option>
							))}
						</select>
						<img className="w-7 h-7" src={getStatus(form.status)?.image} />

						<div className="text-white">{getStatus(form.status)?.text}</div>
						<img className="w-5 h-5" src="/dropdown-white.svg" />
					</div>
					<div
						onClick={onSubmit}
						className="bg-yellow-2 text-white py-3 rounded-md shadow-md flex justify-center cursor-pointer"
					>
						ยืนยัน
					</div>
				</div>
			</div>
		</div>
	)
}

export const getServerSideProps = auth(async ({ locale, query }) => {
	return {
		props: { query },
	}
})
