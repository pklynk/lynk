/* eslint-disable @next/next/no-img-element */
import Navbar from "@components/Navbar"
import Ingredient from "@components/recipe/Ingredient"
import TitleNumber from "@components/TitleNumber"
import axios from "@configs/axios"
import { IRecipe } from "@type"
import { useRouter } from "next/router"
import React, { FC, useEffect, useState } from "react"

const CookingById: FC = () => {
	const router = useRouter()
	const [recipe, setRecipe] = useState<IRecipe>({})

	const [doingIndex, setDoingIndex] = useState<number>(0)
	const [openModal, setOpenModal] = useState<boolean>(false)
	const [comment, setComment] = useState<string>("")

	const nextStep = (index: number) => {
		if (index >= recipe.steps!.length) return

		setDoingIndex(index + 1)

		if (index === recipe.steps!.length - 1) {
			setOpenModal(true)
		}
	}

	const previousStep = (index: number) => {
		if (index <= 0) return
		setDoingIndex(index - 1)
	}

	const closeModal = () => {
		setOpenModal(false)
		setDoingIndex(0)
	}

	const onComment = async () => {
		await axios().post(`/api/v1/recipes/comment?id=${recipe._id}`, {
			text: comment,
		})
		closeModal()
	}

	useEffect(() => {
		if (!router.query.id) return

		axios()
			.get(`/api/v1/guest/${router.query.id}`)
			.then((res) => {
				setRecipe(res.data.recipe)
			})
	}, [router.query])

	return (
		<div className="bg-grey-2 min-h-screen">
			<Navbar></Navbar>
			<div className="px-28 py-14 space-y-7">
				<img
					className="w-full h-96 object-cover rounded-lg shadow-lg"
					src={recipe.image! as string}
				/>
	
				<div className="bg-white relative p-7 space-y-10 rounded-lg shadow-lg">
					<h2 className="text-center text-2xl font-bold">{recipe.title}</h2>
					<div>
						<div>
							<Ingredient ingredients={recipe.ingredients} />
						</div>
					</div>

					<div>
						<div className="space-y-5">
							<div className="font-bold text-xl">ขั้นตอน</div>
							<div className="space-y-14">
								{recipe?.steps?.map((step, i) => (
									<div
										key={i}
										className={`space-y-7 px-8 py-4 rounded-lg shadow-md ${
											doingIndex === i ? "bg-yellow-1" : "bg-white"
										}`}
									>
										<div className="flex space-x-5 ">
											<div>
												<TitleNumber title={i + 1}></TitleNumber>
											</div>

											<div className="">{step.text}</div>
										</div>
										{doingIndex === i && (
											<div className="flex justify-end space-x-5">
												{i !== 0 && (
													<div
														onClick={() => previousStep(i)}
														className="bg-yellow-2 flex items-center justify-between py-2 px-4 rounded-lg  text-white w-36 cursor-pointer"
													>
														<img src="/redo.svg" alt="" />
														<div>ย้อนกลับ</div>
													</div>
												)}

												<div
													onClick={() => nextStep(i)}
													className="bg-yellow-2 flex items-center justify-between py-2 px-4 rounded-lg  text-white w-36 cursor-pointer"
												>
													<img src="/done.svg" alt="" />
													<div>ทำเสร็จแล้ว</div>
												</div>
											</div>
										)}
									</div>
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
			{openModal && (
				<div className="bg-gray-50 flex flex-col justify-between px-28 py-14 w-4/5 h-4/5 fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 rounded-lg shadow-xl">
					<div className="space-y-10">
						<div className="text-center text-4xl">
							ทำอาหารเสร็จแล้วเป็นอย่างไรบ้าง ?
						</div>
						<div className="text-center text-2xl">
							แสดงความคิดเห็นให้เพื่อน ๆ ดูได้เลย
						</div>
					</div>
					<div className="space-y-14">
						<div className="space-y-5">
							<div>แสดงความคิดเห็น</div>
							<textarea
								value={comment}
								onChange={(e) => setComment(e.target.value)}
								className="w-full h-44 focus:outline-none resize-none px-4 py-4 shadow-md rounded-lg"
							></textarea>
						</div>
						<div className="flex justify-end space-x-5">
							<div
								onClick={onComment}
								className="bg-yellow-2 px-6 py-2 rounded-lg text-white cursor-pointer"
							>
								คอมเมนท์
							</div>
							<div
								onClick={closeModal}
								className="bg-grey-1 px-6 py-2 rounded-lg cursor-pointer"
							>
								ยกเลิก
							</div>
						</div>
					</div>
				</div>
			)}
		</div>
	)
}

export default CookingById
