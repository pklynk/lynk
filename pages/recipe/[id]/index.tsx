/* eslint-disable @next/next/no-img-element */
import { useRouter } from "next/router"
import Navbar from "@components/Navbar"
import Ingredient from "@components/recipe/Ingredient"
import Step from "@components/recipe/Step"
import Comment from "@components/recipe/Comment"
import { useEffect, useState } from "react"
import axios from "@configs/axios"
import { IRecipe } from "@type"
import Link from "next/link"
import { getSession } from "next-auth/client"
import getMockAvatar from "@helpers/getMockAvartar"
import { useRecoilState } from "recoil"
import { userState } from "@store"

export default function RecipeById() {
	const router = useRouter()
	const [recipe, setRecipe] = useState<IRecipe>({})
	const [user, setUser] = useRecoilState(userState)

	const fetchRecipe = async () => {
		const session = await getSession()

		axios()
			.get(`/api/v1/${session?.token ? "recipes" : "guest"}/${router.query.id}`)
			.then((res) => {
				setRecipe(res.data.recipe)
			})

		if (session?.token) {
			setUser(session.user!)
		}
	}

	const toggleLike = async () => {
		if (recipe.like) {
			await axios().delete(`/api/v1/recipes/like/${recipe.like._id}`)
			setRecipe((prev) => ({
				...prev,
				like: undefined,
				likeCounts: prev.likeCounts! - 1,
			}))
		} else {
			const res = await axios().post(`/api/v1/recipes/like?id=${recipe._id}`)
			setRecipe((prev) => ({
				...prev,
				like: res.data,
				likeCounts: prev.likeCounts! + 1,
			}))
		}
	}

	useEffect(() => {
		if (!router.query.id) return
		fetchRecipe()
	}, [router.query])

	return (
		<div className="bg-grey-2 min-h-screen">
			<Navbar></Navbar>
			<div className="px-28 py-14 space-y-7">
				<img
					className="w-full h-96 object-cover rounded-lg shadow-lg"
					src={recipe.image! as string}
					alt=""
				/>

				<div className="bg-white relative p-7 space-y-10 rounded-lg shadow-lg ">
					<div>
						<h2 className="text-center text-2xl font-bold">{recipe.title}</h2>
						<div className="absolute top-7 right-7 flex space-x-5">
							{recipe?.like ? (
								<img
									onClick={toggleLike}
									className="w-7 h-7 cursor-pointer"
									src="/heart-black.svg"
									alt=""
								/>
							) : (
								<img
									onClick={toggleLike}
									className="w-7 h-7 cursor-pointer"
									src="/heart-empty.svg"
									alt=""
								/>
							)}

							<img className="w-7 h-7" src="/bookmark-empty.svg" alt="" />
						</div>
					</div>

					<div className="flex justify-between items-center">
						<Link href={`/user/${recipe.user?._id}`}>
							<a className="flex items-center space-x-7">
								<img
									className="w-28 h-28 object-cover rounded-full shadow-lg "
									src={
										recipe.user?.avatar || getMockAvatar(recipe.user?.email!)
									}
									alt=""
								/>
								<div>
									<div>
										{recipe.user?.firstName} {recipe.user?.lastName}
									</div>
									<div>{recipe.user?.username}</div>
								</div>
							</a>
						</Link>
						<div className="flex space-x-3">
							{recipe.user?._id === user?._id ? (
								<Link href={`/recipe/${recipe._id}/edit`}>
									<a className="bg-yellow-2 h-12 flex items-center px-4 rounded-xl shadow-lg space-x-5 cursor-pointer">
										<img src="/edit.svg" alt="" />
										<div className="text-white text-xl">แก้ไข</div>
									</a>
								</Link>
							) : (
								<Link href={`/recipe/create?ref=${recipe._id}`}>
									<a className="bg-yellow-2 h-12 flex items-center px-4 rounded-xl shadow-lg space-x-5 cursor-pointer">
										<img src="/edit.svg" alt="" />
										<div className="text-white text-xl">ดัดแปลง</div>
									</a>
								</Link>
							)}

							<Link href={`/recipe/${recipe._id}/cooking`}>
								<a className="bg-yellow-2 h-12 flex items-center px-4 rounded-xl shadow-lg space-x-5 cursor-pointer">
									<img src="/spoon.svg" alt="" />
									<div className="text-white text-xl">เริ่มทำอาหาร</div>
								</a>
							</Link>
						</div>
					</div>
					<div className="flex justify-between px-5  border-b border-t py-3 ">
						<div className="flex space-x-2">
							<img src="/spoon-black.svg" alt="" />
							<div>สำหรับ {recipe.amount} ที่</div>
						</div>
						<div className="flex space-x-2">
							<img src="/clock.svg" alt="" />
							<div>ระยะเวลา {recipe.duration} นาที</div>
						</div>
						<div className="flex space-x-2">
							<img src="/eye.svg" alt="" />
							<div>อ่าน {recipe.rating?.views} ครั้ง</div>
						</div>
						<div className="flex space-x-2">
							<img src="/heart-black.svg" alt="" />
							<div>{recipe.likeCounts}</div>
						</div>
					</div>
					<Ingredient ingredients={recipe.ingredients}></Ingredient>
					<Step steps={recipe.steps}></Step>
					<Comment comments={recipe.comments} onRefetch={fetchRecipe}></Comment>
				</div>
			</div>
		</div>
	)
}
