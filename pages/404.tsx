/* eslint-disable @next/next/no-img-element */
import Navbar from "../components/Navbar";

export default function NotFound() {
  return <div className="">
      <div className='absolute w-full'>
          <Navbar></Navbar>
      </div>
      <div className='h-screen bg-grey-2 pt-7 flex items-center justify-center'>
          <div>
              <img src="/404.svg" alt=""/>
          </div>
      </div>
  </div>;
}
