import Providers from "next-auth/providers"
import NextAuth from "next-auth"
import axios from "@configs/axios"

export default NextAuth({
	providers: [
		Providers.Credentials({
			name: "credentials",
			credentials: {
				email: { label: "Username", type: "text" },
				password: { label: "Password", type: "password" },
			},
			async authorize(credentials) {
				try {
					const res = await axios().post("/api/v1/auth/sign_in", credentials)
					return res.data
				} catch (error) {
					return null
				}
			},
		}),
	],

	callbacks: {
		async jwt(token: any, authRes: any) {
			if (authRes) {
				token.accessToken = authRes.token
				token.user = authRes.user
			}
			return token
		},
		session(session, token: any) {
			session.token = token.accessToken
			session.user = token.user
			return session
		},
	},
})
