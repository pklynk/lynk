import "../styles/globals.css"
import type { AppProps } from "next/app"
import Head from "next/head"
import { RecoilRoot } from "recoil"

function MyApp({ Component, pageProps }: AppProps) {
	return (
		<RecoilRoot>
			<Head>
				<title>Cookshare</title>
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
				<link rel="icon" href="/logo.svg" type="image/x-icon"></link>
			</Head>
			<Component {...pageProps} />
		</RecoilRoot>
	)
}
export default MyApp
