import { IIngredient, IStep } from "@type"
import { atom } from "recoil"

export const createIngredientStates = atom<IIngredient[]>({
	key: "createIngrendietState",
	default: [],
})

export const createStepStates = atom<IStep[]>({
	key: "createStepState",
	default: [],
})
