import { atom } from "recoil"
import { IUser } from "@type"

export const userState = atom<IUser | undefined>({
	key: "userState",
	default: undefined,
})
