export interface IUser {
	_id?: string
	firstName?: string
	lastName?: string
	email?: string
	username?: string
	avatar?: string
}
export interface IComment {
	text?: string
	image?: string
	user?: IUser
	createdAt?: Date | string
	updatedAt?: Date | string
}
export interface IStep {
	text?: string
	images?: string[]
}
export interface IIngredient {
	name?: string
	image?: string | File
	amount?: number
	unit?: string
}
export interface IRating {
	_id?: string
	recipeId?: string
	score?: number
	views?: number
	createdAt?: Date | string
	updatedAt?: Date | string
}
export interface ILike {
	_id?: string
	text?: string
	userId?: string
	user?: IUser
	createdAt?: Date | string
	updatedAt?: Date | string
}
export interface IRecipe {
	_id?: string
	userId?: string
	user?: IUser
	title?: string
	amount?: number
	duration?: number
	ingredients?: IIngredient[]
	steps?: IStep[]
	public?: boolean
	createdAt?: Date | string
	updatedAt?: Date | string
	rating?: IRating
	comments?: IComment[]
	likeCounts?: number
	image?: string | File
	like?: ILike
	refer?: string
}

export interface ISession {
	user?: IUser
	token?: string
}
