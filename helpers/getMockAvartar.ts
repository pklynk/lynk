export default function getMockAvatar(name: string = "user") {
	return `https://avatars.dicebear.com/api/avataaars/${name}.svg`
}
